<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDO</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

</head>
<body>
    <div class="container">
        <br>
        <br>
    <table>
        <thead>
        <tr>
            <th>id</th>
            <th>modelo</th>
            <th>año</th>
        </tr>
        </thead>
            <tbody>
                <?php
                    $dns = "mysql:host=localhost;dbname=autosdb";
                    $user = "root";
                    $pass = "manuel";

                    try {
                        $con = new PDO($dns, $user, $pass);

                        if(!$con){
                            echo "No se puede conectar a la base de datos";
                        }

                        $query = $con->prepare('SELECT id, modelo, anio FROM lamborghini');

                        $query->execute();

                        while($result = $query->fetch())
                        {
                            ?>


                                <tr>
                                    <td> <?php echo  $result["id"]; ?></td>
                                    <td><?php echo  $result["modelo"]; ?></td>
                                    <td><?php echo  $result["anio"]; ?></td>
                                </tr>

                            <?php

                        }
                    }
                    catch (Exception $e)
                    {
                        echo "Erro: ". $e->getMessage();
                    };
                ?>
            </tbody>
        </table>
    </div>
</body>
</html>